angular.module('testApp')
    .controller('HelloCtrl', [
        '$scope', 'HelloService', '$uibModal',
        function ($scope, HelloService, $uibModal) {
            'use strict';

            $scope.word = "World";

            $scope.hello = function () {
                $uibModal.open({
                    template: '<h1>' + HelloService.greetString($scope.word) + '</h1>',
                    size: 'lg'
                });
            };

        }
    ])
;