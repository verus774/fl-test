angular.module('testApp')
    .factory('HelloService', function () {

    var greetString = function (string) {
        return "Hello, " + string;
    }


    return {
        greetString : greetString
    };

});