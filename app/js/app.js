angular.module('testApp', [
  'templatescache',
  'ngAnimate',
  'ngCookies',
  'ngSanitize',
  'ngMessages',
  'ngAria',
  'ngResource',
  'ui.router',
    'ui.bootstrap'
]).config([
    '$stateProvider',
    '$urlRouterProvider',
    function appConfig($stateProvider, $urlRouterProvider) {
        $stateProvider
            .state("hello", {
                url: "/hello",
                templateUrl: "hello.html",
                controller: "HelloCtrl"
            })
        ;

        $urlRouterProvider.otherwise('/hello');
    }
])

;